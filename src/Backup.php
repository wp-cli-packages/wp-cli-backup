<?php
namespace FiliWP\WP_CLI_Backup;

use WP_CLI;

/**
 * Backups website
 */
class Backup {

	/**
	 * The list of excluded options to exclude from the backup.
	 *
	 * @var array $excludes
	 */
	private array $excludes;

	/**
	 * Sets the excluded options for the backup.
	 *
	 * @param array $exclude An array of options to exclude from the backup.
	 * @return void
	 */
	public function set_exclude( array $exclude ): void {
		$this->excludes = $exclude;
	}

	/**
	 * Retrieves the list of excluded options for the backup.
	 *
	 * This method returns an array of options that should be excluded from the backup process.
	 *
	 * @return array The list of excluded options.
	 */
	public function get_excludes(): array {
		return $this->excludes;
	}

	/**
	 * Checks if a directory is writable.
	 *
	 * @param string $directory_path The path of the directory to check.
	 * @return boolean Returns true if the directory is writable, false otherwise.
	 */
	private function directory_is_writable( string $directory_path ): bool {
		require_once ABSPATH . 'wp-admin/includes/file.php';

		WP_Filesystem();
		global $wp_filesystem;

		return $wp_filesystem->is_writable( $directory_path );
	}

	/**
	 * Checks if a directory path contains a filename.
	 *
	 * @param string $directory_path The directory path to check.
	 * @return boolean Returns true if the directory path contains a filename, false otherwise.
	 */
	private function directory_path_has_filename( string $directory_path ): bool {
		return pathinfo( $directory_path, PATHINFO_EXTENSION ) !== '';
	}

	/**
	 * Checks if the shell_exec function is available.
	 *
	 * @return boolean Returns true if the shell_exec function is available, false otherwise.
	 */
	private function is_shell_exec_available(): bool {
		return function_exists( 'shell_exec' ) && ! in_array(
			'shell_exec',
			array_map(
				'trim',
				explode(
					', ',
					ini_get( 'disable_functions' )
				)
			),
			true
		);
	}

	/**
	 * Prepares the tar exclude options based on the provided array of excluded options.
	 *
	 * @param array $excludes An array of options to be excluded from the tar backup.
	 * @return string The tar exclude options as a string.
	 */
	private function prepare_tar_exclude_options( array $excludes ): string {
		return implode(
			' ',
			array_map(
				function ( $exclude ) {
					return '--exclude=' . escapeshellarg( $exclude );
				},
				$excludes
			)
		);
	}

	/**
	 * Generates a backup filename with the given prefix and file extension.
	 *
	 * @param string $prefix The prefix for the backup filename. Default is 'backup'.
	 * @param string $file_extension The file extension for the backup filename. Default is 'tar.gz'.
	 * @return string The generated backup filename.
	 */
	private function generate_backup_filename( string $prefix = 'backup', string $file_extension = 'tar.gz' ): string {
		$timestamp = gmdate( 'YmdHis' );
		$backup_filename = sprintf( '%s-%s.%s', $prefix, $timestamp, $file_extension );

		return $backup_filename;
	}

	/**
	 * Backups the WordPress website
	 *
	 * ## OPTIONS
	 *
	 * [--backup_dir_path=<backup_dir_path>]
	 * : The WordPress directory path to backup.
	 *
	 * [--destination_dir_path=<destination_dir_path>]
	 * : The backup destination directory path.
	 *
	 * [--exclude=<exclude>]
	 * : The options to exclude from the backup.
	 *
	 * ## EXAMPLES
	 *
	 *    wp backup --backup_dir_path=/var/www/html/wordpress
	 *    --destination_dir_path=/var/www/html/backups
	 *    --exclude=vendor,node_modules
	 *
	 * @param array $args       The arguments for the command.
	 * @param array $assoc_args The associative arguments for the command.
	 *
	 * @return void
	 */
	public function run_backup( array $args, array $assoc_args ): void {
		if ( ! isset( $assoc_args['backup_dir_path'] ) ) {
			WP_CLI::error( 'Provide the WordPress directory path to backup.' );
		}

		$backup_directory_path = $assoc_args['backup_dir_path'];

		if ( $this->directory_path_has_filename( $backup_directory_path ) ) {
			WP_CLI::error( 'Destination directory path should not have a filename.' );
		}

		$destination_directory_path = rtrim( $assoc_args['destination_dir_path'] ?? getcwd(), '/' );

		if ( $this->directory_path_has_filename( $destination_directory_path ) ) {
			WP_CLI::error( 'Destination directory path should not have a filename.' );
		}

		if ( $backup_directory_path === $destination_directory_path ) {
			WP_CLI::error( 'Backup directory and destination directory cannot be the same.' );
		}

		$backup_filename = $this->generate_backup_filename();
		$destination_file_path = $destination_directory_path . '/' . $backup_filename;

		if ( ! $this->directory_is_writable( $destination_directory_path ) ) {
			WP_CLI::error( 'Destination directory is not writable.' );
		}

		$exclude_args = $assoc_args['exclude'] ?? false;
		$exclude = $exclude_args ? array_unique( explode( ',', str_replace( ' ', '', $exclude_args ) ) ) : array();

		$this->set_exclude( $exclude );

		$exclude_options = $this->prepare_tar_exclude_options( $this->get_excludes() );

		$tar_command = "tar -vczf {$destination_file_path} -C {$backup_directory_path} {$exclude_options} .";

		if ( ! $this->is_shell_exec_available() ) {
			WP_CLI::error( 'Shell exec is not available.' );
		}

		WP_CLI::log( 'Creating WordPress backup...' );
		WP_CLI::log( 'Running command: ' . $tar_command );

		$output = shell_exec( $tar_command );

		if ( empty( $output ) ) {
			WP_CLI::error( 'Failed to create WordPress backup.' );
		}

		if ( ! file_exists( $destination_file_path ) ) {
			WP_CLI::error( 'Backup file does not exist.' );
		}

		$backup_file_size = filesize( $destination_file_path );

		WP_CLI::log( 'Backup file created: ' . $destination_file_path );
		WP_CLI::log( 'Backup file size: ' . size_format( $backup_file_size ) );

		WP_CLI::success( 'WordPress backup created successfully.' );
	}
}
